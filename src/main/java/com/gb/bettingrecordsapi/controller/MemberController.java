package com.gb.bettingrecordsapi.controller;

import com.gb.bettingrecordsapi.model.member.MemberCreateRequest;
import com.gb.bettingrecordsapi.model.member.MemberEtcMemoChangeRequest;
import com.gb.bettingrecordsapi.model.member.MemberItem;
import com.gb.bettingrecordsapi.model.member.MemberResponse;
import com.gb.bettingrecordsapi.service.MemberService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member")
public class MemberController {
    private final MemberService memberService;

    @PostMapping("/join")
    public String setMember(@RequestBody MemberCreateRequest request) {
        memberService.setMember(request);

        return "등록 완료";
    }

    @GetMapping("/all")
    public List<MemberItem> getMembers() {
        return memberService.getMembers();
    }

    @GetMapping("/detail/{id}")
    public MemberResponse getMember(@PathVariable long id) {
        return memberService.getMember(id);
    }

    @PutMapping("/etc-memo/{id}")
    public String putMemberEtcMemo(@PathVariable long id, @RequestBody MemberEtcMemoChangeRequest request) {
        memberService.putMemberEtcMemo(id, request);
        return "수정 완료";
    }
}
