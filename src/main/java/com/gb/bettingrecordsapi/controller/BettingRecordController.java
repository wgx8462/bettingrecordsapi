package com.gb.bettingrecordsapi.controller;

import com.gb.bettingrecordsapi.entity.Member;
import com.gb.bettingrecordsapi.model.bettingrecord.BettingRecordCreateRequest;
import com.gb.bettingrecordsapi.model.bettingrecord.BettingRecordItem;
import com.gb.bettingrecordsapi.model.bettingrecord.BettingRecordPriceChangeRequest;
import com.gb.bettingrecordsapi.model.bettingrecord.BettingRecordResponse;
import com.gb.bettingrecordsapi.service.MemberService;
import com.gb.bettingrecordsapi.service.BettingRecordService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/betting-record")
public class BettingRecordController {
    private final MemberService memberService;
    private final BettingRecordService bettingRecordService;

    @PostMapping("/new/member-id/{memberId}")
    public String setBetting(@PathVariable long memberId, @RequestBody BettingRecordCreateRequest request) {
        Member member = memberService.getDate(memberId);
        bettingRecordService.setBetting(member, request);

        return "OK";
    }

    @GetMapping("/all")
    public List<BettingRecordItem> getBettings() {
        return bettingRecordService.getBettings();
    }

    @GetMapping("/detail/betting-record-id/{bettingRecordId}")
    public BettingRecordResponse getBetting(@PathVariable long bettingRecordId) {
        return bettingRecordService.getBetting(bettingRecordId);
    }

    @PutMapping("/price/betting-record-id/{bettingRecordId}")
    public String putBettingPrice(@PathVariable long bettingRecordId, @RequestBody BettingRecordPriceChangeRequest request) {
        bettingRecordService.putBettingPrice(bettingRecordId, request);

        return "수정 완료";
    }

    @DeleteMapping("/betting-record-id/{bettingRecordId}")
    public String delBetting(@PathVariable long bettingRecordId) {
        bettingRecordService.delBetting(bettingRecordId);

        return "삭제 완료";
    }

}
