package com.gb.bettingrecordsapi.repository;

import com.gb.bettingrecordsapi.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MemberRepository extends JpaRepository<Member, Long> {
}
