package com.gb.bettingrecordsapi.repository;

import com.gb.bettingrecordsapi.entity.BettingRecord;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BettingRecordRepository extends JpaRepository<BettingRecord, Long> {
}
