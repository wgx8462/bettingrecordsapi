package com.gb.bettingrecordsapi.service;

import com.gb.bettingrecordsapi.entity.BettingRecord;
import com.gb.bettingrecordsapi.entity.Member;
import com.gb.bettingrecordsapi.model.bettingrecord.BettingRecordCreateRequest;
import com.gb.bettingrecordsapi.model.bettingrecord.BettingRecordItem;
import com.gb.bettingrecordsapi.model.bettingrecord.BettingRecordPriceChangeRequest;
import com.gb.bettingrecordsapi.model.bettingrecord.BettingRecordResponse;
import com.gb.bettingrecordsapi.repository.BettingRecordRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class BettingRecordService {
    private final BettingRecordRepository bettingRecordRepository;

    public void setBetting(Member member, BettingRecordCreateRequest request) {
        BettingRecord addData = new BettingRecord();

        addData.setMember(member);
        addData.setDatePrize(LocalDate.now());
        addData.setPrice(request.getPrice());

        bettingRecordRepository.save(addData);
    }

    public List<BettingRecordItem> getBettings() {
        List<BettingRecord> originList = bettingRecordRepository.findAll();
        List<BettingRecordItem> result = new LinkedList<>();

        for (BettingRecord bettingRecord : originList) {
            BettingRecordItem addItem = new BettingRecordItem();
            addItem.setId(bettingRecord.getId());
            addItem.setMemberName(bettingRecord.getMember().getName());
            addItem.setPrice(bettingRecord.getPrice());
            result.add(addItem);
        }
        return result;
    }

    public BettingRecordResponse getBetting(long id) {
        BettingRecord originData = bettingRecordRepository.findById(id).orElseThrow();
        BettingRecordResponse response = new BettingRecordResponse();

        response.setMemberId(originData.getMember().getId());
        response.setMemberName(originData.getMember().getName());
        response.setDatePrize(originData.getDatePrize());
        response.setPrice(originData.getPrice());

        return response;
    }

    public void putBettingPrice(long id, BettingRecordPriceChangeRequest request) {
        BettingRecord originData = bettingRecordRepository.findById(id).orElseThrow();
        originData.setPrice(request.getPrice());

        bettingRecordRepository.save(originData);
    }

    public void delBetting(long id) {
        bettingRecordRepository.deleteById(id);
    }
}
