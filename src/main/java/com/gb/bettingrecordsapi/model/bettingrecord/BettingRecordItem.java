package com.gb.bettingrecordsapi.model.bettingrecord;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BettingRecordItem {
    private Long id;
    private String memberName;
    private Double price;
}
