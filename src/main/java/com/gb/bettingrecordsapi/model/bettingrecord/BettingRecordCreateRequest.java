package com.gb.bettingrecordsapi.model.bettingrecord;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class BettingRecordCreateRequest {
    private LocalDate datePrize;
    private Double price;
}
