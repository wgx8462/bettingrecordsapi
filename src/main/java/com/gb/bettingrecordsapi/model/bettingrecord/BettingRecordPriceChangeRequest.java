package com.gb.bettingrecordsapi.model.bettingrecord;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BettingRecordPriceChangeRequest {
    private Double price;
}
