package com.gb.bettingrecordsapi.model.bettingrecord;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class BettingRecordResponse {
    private Long memberId;
    private String memberName;
    private LocalDate datePrize;
    private Double price;
}
