package com.gb.bettingrecordsapi.model.member;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class MemberItem {
    private String name;
    private LocalDate dateBirth;
}
