package com.gb.bettingrecordsapi.model.member;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MemberEtcMemoChangeRequest {
    private String etcMemo;
}
